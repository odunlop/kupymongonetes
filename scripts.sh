minikube delete # make sure we haven't got anything already running
minikube start --nodes 3
kubectl apply -f k8s/tasksapp.yaml
kubectl apply -f k8s/tasksapp-svc.yaml
kubectl apply -f k8s/mongo-pv.yaml
kubectl apply -f k8s/mongo-pvc.yaml
kubectl apply -f k8s/mongo.yaml
minikube addons enable metrics-server # for debugging 
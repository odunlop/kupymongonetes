# MongoDB in Flask in Kubernetes
This repo was me figuring out how to run a Flask app using MongoDB as its storage inside a Kubernetes cluster. To achieve this I followed [this](https://levelup.gitconnected.com/deploy-your-first-flask-mongodb-app-on-kubernetes-8f5a33fa43b4) tutorial, with some adjustments within the `app.py` file and the Persistent Volumes files.


## Set-Up
The scripts used to manage the k8s cluster use **minikube** - so make sure you have this installed (note: minikube is a bit funny with the M1 chip so make sure to be wary when looking to Google for how to download. I'd provide a link but honestly forgot which guidance I followed). 

*(Further down is a section on how to run this locally for testing)*
### First Stage: Docker
1. Create a docker image of the app via `docker build --tag <dockerhub-username>/<app-name>`
    * Make sure to tag it in this username/app-name format so you can push to your DockerHub repo
    * If you want, you can skip the docker steps and just use mine `orladunlop/mongo-demo-app`
        * I'll use this image going forward
2. Push to DockerHub so it's accessible by our pods when we're deploying: `docker push orladunlop/mongo-demo-app` (REMEMBER: you must have the desktop Docker app running and be logged in to push to DockerHub)

### Second Stage: Kubernetes
For time-keeping when I was in a debugging hate-loop, I put all of my Kubernetes scripts in a file so I could just run `zsh scripts.sh` everytime I wanted to burn and rebuild. So you can do that, or just follow this:
1. `minikube start --nodes 3` - starts the cluster with one master node and two worker nodes.
2. `kubectl apply -f k8s/tasksapp.yaml` - deploys 3 replicas of our app via the Docker image we made. (Pls ignore that the files still say **tasksapp** even though it's no longer a tasks app, I'm lazy)
3. `kubectl apply -f k8s/tasksapp-svc.yaml` - creates a service for our app
    * You can actually include this the deployment file and this is apparently usual practice - you can see how you can do that in our MongoDB deployment
4. `kubectl apply -f k8s/mongo-pv.yaml` - creates 256 MB of storage which is made avaliable to our future Mongo container. 
    * The contents of this persists even if the MongoDB pod is deleted, moved or dies
    * `kubectl get pv` to check it's worked - should see the status is `Avaliable` 
5. `kubectl apply -f k8s/mongo-pvc.yaml` - creates a PersistentVolumeClaim which is used to claim/obtain the storage we just created and can be mounted on our future Mongo container. Basically, it's how we get our Mongo container to attach to the storage we created in **Step 4** - in the `mongo.yaml` deployment file you can see that we reference `mongo-pvc` in the volumes section.
    * I made an adjustment in this file from the tutorial as the `mongo-pvc` was not connecting with my `mongo-pv` which I remedied by adding these lines under `spec`:
    ```
    storageClassName: ""
    volumeName: mongo-pv
    ```
6. `kubectl apply -f k8s/mongo.yaml` - deploying our Mongo container and service all in one.
    * Important to note!!! The service **must** be called "mongo" otherwise your database won't work. This means when you view your app in localhost, the page will fail to load and subsequently crash.

### Third Stage: It Lives?
And now you have it all deployed! All that's left to do is access it, woo! 

If you run `kubectl get services` you'll see that our `app-svc` service's `EXTERNAL-IP` is still in `pending` :anguished: but that's okay! We simpy need to run `minikube tunnel` to get our LoadBalancer to expose ports for access at `http://localhost:8080` :tada:

### Local test
1. Install the following
    * Flask
    * Flask-PyMongo
2. Change the `MONGO_URI` url to `mongodb://localhost:27017/dev`
3. Run `flask run`


## Debugging Tips
* Whilst running `minikube tunnel` in one terminal, you can view the live logs of a pod via `kubectl logs -f [pod name]`
    * Take the `-f` flag off if you just wanted most-recent logs printed in the terminal
* `minikube addons enable metrics-server` allows us to view our cluster's metrics, including running commands like `kubectl top pod` which shows you the memory and CPU your pods are using! 



from flask import (
    Flask, request, redirect, render_template, url_for
)
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
import socket

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://mongo:27017/dev"
# app.config["MONGO_URI"] = "mongodb://localhost:27017/dev"
mongo = PyMongo(app)
db = mongo.db

@app.route('/')
def index():
    pod = socket.gethostname()
    messages = db.message.find()
    return render_template('index.html', pod=pod, messages=messages)

@app.route('/new', methods=['POST'])
def new_task():
    db.message.insert_one({'message': request.form['message'] })
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)